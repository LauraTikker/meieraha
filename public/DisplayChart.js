function expandChart(chartData) {

  var chartDataForExpanded = _.cloneDeep(chartData);


  var subArray = chartDataForExpanded.map(function (link) {
    return link.sub
  })

  var subsUnpacked = unPackSubs(subArray)


  var expandedChartData = chartDataForExpanded.concat(subsUnPacked)

  var expandedChartDataHoverInfo = expandedChartData.map(function (link) {
    var idIndex = link.id.substring(0, 3)
    if(link.clickable) {
      if (idIndex == "Ex2") {
        link.clickable = am4core.MouseCursorStyle.pointer;
        link.hoverState = 1
      } else {
        link.clickable = am4core.MouseCursorStyle.default;
        link.hoverState = 0.8
      }
    }
    return link
  })

  return expandedChartDataHoverInfo
}

function createChart(chartData, chartDataExpanded) {

  var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  am4core.options.autoSetClassName = true;

  var chart = am4core.create("chart-container", am4charts.SankeyDiagram);

  chartProperties(chart)

  createLinkTemplate(chart)

  createNodeTemplate(chart)

  var nav = addNavigationBar(chart)

  chart.responsive.rules.push({
    relevant: function(target) {

      if (target.pixelWidth >= 690) {
        $('#chart-container').css('height', '810px')
        nav.data = [{ name: "Riigieelarve"}]

        chart.nodes.template.nameLabel.label.adapter.add("textAlign", function (textAlign, target) {
          switch (target.parent.parent.level) {
            case 0:
              return "start";
            case 1:
              return "start";
            case 2:
              return "end";
            case 3:
              return "end";
            case 4:
              return "end";
          }
        });

        chart.nodes.template.nameLabel.adapter.add("locationX", function (location, target) {
          switch (target.parent.level) {
            case 0:
              return 1;
            case 1:
              return 1;
            case 2:
              return -40;
            case 3:
              return -40;
            case 4:
              return -40;
          }
        });

        return true;
      }

      return false;
    },
    state: function(target, stateId) {

      if (target instanceof am4charts.Chart) {
        var state = target.states.create(stateId);
        state.properties.height = 800;
        state.properties.data = chartDataExpanded;

        return state;

      }

      return null;
    }
  });

  chart.responsive.rules.push({
    relevant: function(target) {
      if (target.pixelWidth < 690) {
        return true;
      }
      return false;
    },
    state: function(target, stateId) {

      if (target instanceof am4charts.Chart) {
        var state = target.states.create(stateId);
        state.properties.data = chartData;

        return state;
      }
      return null;
    }
  });

  //functions to navigate the chart when links are clicked (by recreating chart depending on click location)
  chart.links.template.events.on("hit", function(ev) {

    var linkData = ev.target.dataItem.dataContext;
    chartClickHandler(chart, nav, linkData, chartData, chartDataExpanded, ev)

  });

  chart.nodes.template.nameLabel.events.on("hit", function(ev)  {
    var linkData = ev.target.dataItem.dataContext;
    chartClickHandler(chart, nav, linkData, chartData, chartDataExpanded, ev)

  })

  nav.links.template.events.on("hit", function(ev) {
    navBarClicked(chartData, chartDataExpanded, chart, ev)
  });

  return {
    chart: chart,
    nav: nav
  }

}

function updateChart(chartData, chartDataExpanded, chartObject) {

  var chart = chartObject.chart

  chart.children.getIndex(0).dispose()

  var newNav = addNavigationBar(chart)

  newNav.data = [{ name: "Riigieelarve"}];
  var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  if (screenWidth > 768) {
    chart.height = 800;
    chart.data = chartDataExpanded;
    newNav.data = [{ name: "Riigieelarve"}]

  }
  else {
    chart.data = chartData;
  }

  chartProperties(chart)
  chart.responsive.rules.push({
    relevant: function(target) {
      if (target.pixelWidth >= 690) {
        $('#chart-container').css('height', '810px')
        newNav.data = [{ name: "Riigieelarve"}]

        chart.nodes.template.nameLabel.label.adapter.add("textAlign", function (textAlign, target) {
          switch (target.parent.parent.level) {
            case 0:
              return "start";
            case 1:
              return "start";
            case 2:
              return "end";
            case 3:
              return "end";
            case 4:
              return "end";
          }
        });

        chart.nodes.template.nameLabel.adapter.add("locationX", function (location, target) {
          switch (target.parent.level) {
            case 0:
              return 1;
            case 1:
              return 1;
            case 2:
              return -40;
            case 3:
              return -40;
            case 4:
              return -40;
          }
        });

        return true;
      }
      return false;
    },
    state: function(target, stateId) {

      if (target instanceof am4charts.Chart) {
        var state = target.states.create(stateId);
        state.properties.height = 800;
        state.properties.data = chartDataExpanded;

        return state;
      }
      return null;
    }
  });

  chart.responsive.rules.push({
    relevant: function(target) {
      if (target.pixelWidth < 690) {
        return true;
      }
      return false;
    },
    state: function(target, stateId) {


      if (target instanceof am4charts.Chart) {
        var state = target.states.create(stateId);
        state.properties.data = chartData;

        return state;
      }
      return null;
    }
  });

  chart.links.template.events.off("hit")

  //functions to navigate the chart when links are clicked (by recreating chart depending on click location)
  chart.links.template.events.on("hit", function(ev) {

    var linkData = ev.target.dataItem.dataContext;
    chartClickHandler(chart, newNav, linkData, chartData, chartDataExpanded, ev)

  });

  chart.nodes.template.nameLabel.events.off("hit")

  chart.nodes.template.nameLabel.events.on("hit", function(ev)  {

    var linkData = ev.target.dataItem.dataContext;
    chartClickHandler(chart, newNav, linkData, chartData, chartDataExpanded, ev)

  })

  newNav.links.template.events.on("hit", function(ev) {
    navBarClicked(chartData, chartDataExpanded, chart, ev)

  });
}

function chartProperties(chart) {

  chart.dataFields.fromName = "from";
  chart.dataFields.toName = "to";
  chart.dataFields.value = "value";
  chart.dataFields.color = "nodeColor";
  chart.dataFields.tooltipHTML = "tooltip";

  chart.paddingRight = 10;
  chart.height = 800;
  chart.paddingLeft = 0;
  chart.paddingBottom = 0;
  chart.minNodeSize = 0.001;
  chart.nodePadding = 22;
  chart.nodeAlign = "top";

  chart.responsive.enabled = true;

  chart.tooltip.getFillFromObject = false;
  chart.tooltip.strokeWidth = 0
  chart.tooltip.getStrokeFromObject = false;
  chart.tooltip.background.fill = am4core.color("#212529");
  chart.tooltip.background.stroke = am4core.color("#212529");
  chart.tooltip.label.interactionsEnabled = true;
  chart.tooltip.keepTargetHover = true;

  var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  // Seda kasutada, et saada viis või kolm levelit, kasutada siis erinevat ChartDatat
  chart.responsive.rules.push({
    relevant: function(target) {
      if (target.pixelWidth < 690) {
        return true;
      }
      return false;
    },
    state: function(target, stateId) {
      if (target instanceof am4charts.Chart) {
        var state = target.nodes.template.nameLabel.states.create(stateId);
        state.properties.fontWeight = 'normal';
        state.properties.fontSize = 12;

        return state;
      }
      return null;
    }
  });

  $('#chart-container').css('height', '810px')
}

function createLinkTemplate(chart)  {

  var linkTemplate = chart.links.template;
  linkTemplate.controlPointDistance = 0.0;
  linkTemplate.tension = 0.4;
  //linkTemplate.stroke = am4core.color('white');
  linkTemplate.colorMode = "solid";
  linkTemplate.fillOpacity = 0.8;
  linkTemplate.strokeOpacity = 0.8;
  linkTemplate.propertyFields.fill = "nodeColor"

  linkTemplate.tooltipHTML = "{tooltipHTML}";

  linkTemplate.propertyFields.cursorOverStyle = 'clickable'
  let hoverState = linkTemplate.states.create("hover")

  hoverState.propertyFields.fillOpacity = 'hoverState';

  // linkTemplate.tooltipHTML.getFillFromObject = false; // no error
  // linkTemplate.tooltip.background.fill = am4core.color("#CEB1BE"); //TypeError: linkTemplate.tooltip is undefined
  //linkTemplate.tooltipHTML.background.fill = am4core.color("#CEB1BE"); //TypeError: linkTemplate.tooltipHTML.background is undefined
  // linkTemplate.tooltipHTML.fill = am4core.color("#17202a");
  // linkTemplate.cursorOverStyle = am4core.MouseCursorStyle.pointer;
}

function createNodeTemplate(chart) {

  var nodeTemplate = chart.nodes.template;

  nodeTemplate.width = 5;
  nodeTemplate.nameLabel.height = 88;
  // nodeTemplate.nameLabel.label.fill = am4core.color("black");
  // nodeTemplate.nameLabel.fontWeight = 'bold'
   nodeTemplate.nameLabel.fontSize = 14;
//  nodeTemplate.nameLabel.lineHeight = 0.5;
  nodeTemplate.clickable = false;
  nodeTemplate.draggable = false;
  // nodeTemplate.nameLabel.label.maxWidth = 200;
  nodeTemplate.nameLabel.label.minWidth = 200;
  nodeTemplate.nameLabel.label.truncate = false;
  nodeTemplate.nameLabel.label.wrap = true;
  nodeTemplate.nameLabel.label.labelWidth = 50;
  nodeTemplate.nameLabel.outline = "#ffcc80"
  nodeTemplate.nameLabel.propertyFields.cursorOverStyle = 'clickable'

  var w = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  nodeTemplate.nameLabel.label.adapter.add("textAlign", function (textAlign, target) {
    if (w <= 768) {
      switch (target.parent.parent.level) {
        case 0:
          return "start";
        case 1:
          return "end";
        case 2:
          return "end";
      }
    } else {
      switch (target.parent.parent.level) {
        case 0:
          return "start";
        case 1:
          return "start";
        case 2:
          return "end";
        case 3:
          return "end";
        case 4:
          return "end";
      }
    }
  });

  nodeTemplate.nameLabel.adapter.add("locationX", function (location, target) {

    if (w <= 768) {
      switch (target.parent.level) {
        case 0:
          return 1;
        case 1:
          return -40;
        case 2:
          return -40;
      }
    } else {
      switch (target.parent.level) {
        case 0:
          return 1;
        case 1:
          return 1;
        case 2:
          return -40;
        case 3:
          return -40;
        case 4:
          return -40;
      }
    }
  });

}

function makeNavBarArrayLevelOne(nav, name, chartData, costOrExpense)  {

  var navDataLiikmed = nav.data.filter(function (one) {
    return one.name === name
  })

  if (navDataLiikmed.length == 0) {

    nav.data.push({
      name: name,
      chartData: chartData
    });
    nav.invalidateData();
  }
}

function makeNavBarArraySubLevels(nav, name, levelName, chartData) {
  var navDataLiikmed = nav.data.filter(function (one) {
    var nameArray = one.name.split("→")

    return name === nameArray[0]
  })

  if (navDataLiikmed.length == 0) {

    nav.data.push({
      name: name + "→" + levelName,
      chartData: chartData
    });
    nav.invalidateData();
  }
}

function makeChartDataSubLevels(chart, item) {
  var siblingsArray = item.sub

//extract ex3 items from sub arrays in correct ex2 and unpack them into onedimensional array
  var siblingSubsArray = siblingsArray.map(function (subItem) {
    return subItem.sub
  })

  var siblingSubsUnpacked = unPackSubs(siblingSubsArray)

  //add both ex2 and ex3 level items into one array to be used as chart data
  var newChartData = siblingsArray.concat(siblingSubsUnpacked)

  chart.height = 400;
  $('#chart-container').css('height', '410px')
  chart.data = newChartData;
  return newChartData;
}

function addNavigationBar(chart) {

  var nav = chart.createChild(am4charts.NavigationBar);
  nav.data = [{ name: "Riigieelarve" }];
  nav.toBack();

  return nav;
}

function navBarClicked(chartData, chartDataExpanded, chart, ev) {

  var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  var target = ev.target.dataItem.dataContext;
  var nav = ev.target.parent;

  if (screenWidth < 768) {

    if (target.name == "Kulud" || target.name == "Tulud") {
      chart.height = 1000;
      $('#chart-container').css('height', '1010px')
    } else if (target.name == "Riigieelarve") {
      chart.height = 800;
      $('#chart-container').css('height', '810px')
    } else {
      chart.height = 400;
      $('#chart-container').css('height', '410px')
    }

    if (target.chartData) {

      chart.data = target.chartData;

      if (target.name == 'Tulud') {
      } else {
        nav.data.splice(nav.data.indexOf(target) + 1);
      }
      nav.invalidateData();
    } else {
      chart.data = chartData;
      nav.data = [{name: "Riigieelarve"}];
    }
  } else {
    if (target.name == "Kulud koos programmidega")  {

    } else {

      var newChartDataHoverInfo = chartDataExpanded.map(function (link) {
        var idIndex = link.id.substring(0, 3)
        if (link.clickable) {
          if (idIndex == "Ex2") {
            link.clickable = am4core.MouseCursorStyle.pointer;
            link.hoverState = 1
          } else {
            link.clickable = am4core.MouseCursorStyle.default;
            link.hoverState = 0.8
          }
        }
        return link
      })

      chart.data = newChartDataHoverInfo
      nav.data = [{ name: "Riigieelarve"}]
    }
  }
}

function unPackSubs(subsArray) {
  subsUnPacked = []

  for (i = 0; i < subsArray.length; i++) {
    for (j = 0; j < subsArray[i].length; j++) {
      subsUnPacked.push(subsArray[i][j])
    }
  }

  return subsUnPacked
}

function findParentOfClickedLink(levelNumber, oneSideOfData, linkData) {

  if (levelNumber == 2) {
    if (linkData.id.charAt(0) == "E") {
      var parentItem = oneSideOfData.find(function (link) {
        return link.to == linkData.from
      })
    }
    if (linkData.id.charAt(0) == "I") {
      var parentItem = oneSideOfData.find(function (link) {
        return link.from == linkData.to
      })
    }
    return parentItem
  }

  else {
    var subsArray = oneSideOfData.map(function (link) {
      return link.sub
    })

    var subsUnPacked = unPackSubs(subsArray)

    return findParentOfClickedLink(levelNumber - 1, subsUnPacked, linkData)
  }
}

function createNewChartBasedOnClickedLink(clickTargetLevel, oneSideOfData, linkData, levelName, ev, nav, chart) {

  var parentItem = findParentOfClickedLink(clickTargetLevel, oneSideOfData, linkData)

  //if navbar has more members than the level of the link clicked (click is on the left side of the expenses chart), navigate one step back
  if ((nav.data.length - clickTargetLevel) > 0) {

    chart.data = nav.data[(nav.data.length - 2)].chartData

    var array = nav.data.splice(0, (nav.data.length - 1))
    nav.data = array

    if (clickTargetLevel == 2)  {
      chart.height = 1000;
      $('#chart-container').css('height', '1010px')
    }
    else {
      chart.height = 400;
      $('#chart-container').css('height', '410px')
    }
  }

  else if ((clickTargetLevel - nav.data.length) == 0) {
    var ifNextLevelExists = false

    for (i = 0; i < parentItem.sub.length; i++)  {
      if (parentItem.sub[i].sub.length > 0) {
        ifNextLevelExists = true
        break
      }
    }

    if (ifNextLevelExists) {

      var newChartData = makeChartDataSubLevels(chart, parentItem)
      makeNavBarArraySubLevels(nav, ev.target.populateString("{fromName}"), levelName, newChartData)
    }
  }
}

function chartClickHandler(chart, nav, linkData, chartData, chartDataExpanded, ev) {
  var costOrExpense = linkData.id.charAt(0);
  var clickTargetLevel = linkData.id.charAt(2);

  var oneSideOfData = chartData.filter(function (node) {
    return node.id.charAt(0) == costOrExpense;
  });

  var screenWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);

  if (screenWidth < 768) {
    if (clickTargetLevel == "1") {

      if (nav.data.length == 1) {
        var subsArray = oneSideOfData.map(function (link) {
          return link.sub
        })

        var subsUnPacked = unPackSubs(subsArray);

        var newChartData = oneSideOfData.concat(subsUnPacked)


        chart.data = newChartData;

        if (costOrExpense == "E") {
          chart.height = 1000;
          $('#chart-container').css('height', '1010px')

          chart.nodes.template.nameLabel.label.adapter.add("textAlign", function (textAlign, target) {

            switch (target.parent.parent.level) {
              case 0:
                return "start";
              case 1:
                return "end";
              case 2:
                return "end";
            }
          });

          chart.nodes.template.nameLabel.adapter.add("locationX", function (location, target) {

            switch (target.parent.level) {
              case 0:
                return 1;
              case 1:
                return -40;
              case 2:
                return -40;
            }
          });
          makeNavBarArrayLevelOne(nav, "Kulud", newChartData, costOrExpense)
        } else if (costOrExpense == "I") {

          chart.height = 1000;
          $('#chart-container').css('height', '1010px')

          chart.nodes.template.nameLabel.label.adapter.add("textAlign", function (textAlign, target) {

            switch (target.parent.parent.level) {
              case 0:
                return "start";
              case 1:
                return "start";
              case 2:
                return "end";
            }
          });

          chart.nodes.template.nameLabel.adapter.add("locationX", function (location, target) {

            switch (target.parent.level) {
              case 0:
                return 1;
              case 1:
                return 1;
              case 2:
                return -40;
            }
          });

          makeNavBarArrayLevelOne(nav, "Tulud", newChartData, costOrExpense)
        }
      }

      //if already navigated to second level, go back to beginning when clicking on level 1 link
      else if (nav.data.length > 1) {
        chart.data = chartData
        chart.height = 800;
        $('#chart-container').css('height', '810px')
        nav.data = [{name: "Riigieelarve"}]
      }
    }

    if (clickTargetLevel == "2") {

      createNewChartBasedOnClickedLink(2, oneSideOfData, linkData, "Programmid", ev, nav, chart)
    }

    if (clickTargetLevel == "3") {

      createNewChartBasedOnClickedLink(3, oneSideOfData, linkData, "Meetmed", ev, nav, chart)
    }

    if (clickTargetLevel == "4") {

      createNewChartBasedOnClickedLink(4, oneSideOfData, linkData, "Alammeetmed", ev, nav, chart)
    }
  } else {

    if (clickTargetLevel == "2" && costOrExpense == "E") {

      if (nav.data.length == 1) {
        var chartDataNoIn2 = chartDataExpanded.filter(function (link) {
          var substring = link.id.substring(0, 3)
          return substring != "In2"
        })

        var ex2Array = chartDataExpanded.filter(function (link) {
          var substring = link.id.substring(0, 3);
          return substring == "Ex2"
        })

        var ex2SubsArray = ex2Array.map(function (link) {
          return link.sub
        })

        var ex2SubsArrayUnpacked = unPackSubs(ex2SubsArray)

        var newChartData = chartDataNoIn2.concat(ex2SubsArrayUnpacked)

        var newChartDataHoverInfo = newChartData.map(function (link) {
          var idIndex = link.id.substring(0, 3)
          if (link.clickable) {
            if (idIndex == "In1") {
              link.clickable = am4core.MouseCursorStyle.pointer;
              link.hoverState = 1
            } else {
              link.clickable = am4core.MouseCursorStyle.default;
              link.hoverState = 0.8
            }
          }
          return link
        })


        chart.data = newChartDataHoverInfo
        nav.data.push({name: "Kulud koos programmidega"})
        nav.invalidateData();
      }
    } else if (clickTargetLevel == "1" && costOrExpense == "I") {

      if (nav.data.length > 1) {

        var newChartDataHoverInfo = chartDataExpanded.map(function (link) {
          var idIndex = link.id.substring(0, 3)
          if (link.clickable) {
            if (idIndex == "Ex2") {
              link.clickable = am4core.MouseCursorStyle.pointer;
              link.hoverState = 1
            } else {
              link.clickable = am4core.MouseCursorStyle.default;
              link.hoverState = 0.8
            }
          }
          return link
        })

        chart.data = newChartDataHoverInfo

        nav.data = [{name: "Riigieelarve"}]
      }

    }

  }

}